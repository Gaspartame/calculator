"use strict"

let exp = (a, b) => a**b;
let multiply = (a, b) => a * b;
let divide = (a, b) => b != 0 ? a / b : NaN;
let add = (a, b) => a + b;
let substract = (a, b) => a - b;

let operators = ["-", "+", "/", "*", "**"];

function op2(operator, a, b) {
    console.log(`operating ${a}${operator}${b}`);
    switch (operator) {
        case "**":
            return exp(a,b);
            break;
        case "*":
            return multiply(a,b);
            break;
        case "/":
            return divide(a,b);
            break;
        case "+":
            return add(a,b);
            break;
        case "-":
            return substract(a,b);
            break;
        default:
            console.error("bad operator " + operator);
            break;
    }
}

function evaluate(str) {
    console.log("evaluating " + str)
    if (str.length == 0) {
        console.error("bout de chaine");
    } else if (!isNaN(str)) {
        return +str;
    } else {
        for (let op of operators) {
            if (str.includes(op)) {
                let tail = str.split(op);
                let head = tail.pop();
                tail = tail.join(op);
                if (op == "*" && tail[tail.length - 1] == op) {
                    continue;
                }
                console.log(`gonna send to op2 ${tail}|${op}|${head}`);
                return op2(op, evaluate(tail), evaluate(head));
            }
        }
    }
    console.error("can't evaluate " + str);
}

let input = document.querySelector("input");
let output = document.getElementById("result");

function calculate() {
    output.innerText = evaluate(input.value);
}

let touches = document.querySelectorAll(".grid>div");
for (let touche of touches) {
    if ([...touche.classList].includes("clear")) {
        touche.addEventListener("click", e => {
            input.value = "";
            calculate();
        })
    } else if ([...touche.classList].includes("backspace")) {
        touche.addEventListener("click", e => {
            input.value = input.value.slice(0, -1);
            calculate();
        })
    } else {
        touche.addEventListener("click", e => {
            input.value += touche.innerText;
            calculate();
        })
    }
}
